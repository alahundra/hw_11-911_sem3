using System;
using System.Collections.Generic;
using System.Text;

namespace MassaApp
{
class Skorost
{
private readonly decimal metr_sec;

public decimal Metr_sec
{
get => metr_sec;
}

public decimal Km_ch
{
get => metr_sec / 1000 * 3600;               //переводим в км в час
}
public decimal Mil_ch
{
get => metr_sec / 1000 * 3600 / 1.609m;     //переводим в мили в час
}

private Skorost(decimal g)
{
metr_sec = g;
}

static public Skorost CreateFromMetr_sec(decimal g)
{
return new Skorost(g);
}

static public Skorost CreateFromKm_ch(decimal g)
{
return new Skorost(g * 1000 / 3600);                         
}
static public Skorost CreateFromMil_ch(decimal g)
{
return new Skorost(g * 1000 / 3600 * 1.609m);
}

static public Skorost operator +(Skorost g1, Skorost g2)
{
return new Skorost(g1.Metr_sec + g2.Metr_sec);
}

static public Skorost operator -(Skorost g1, Skorost g2)
{
return new Skorost(g1.Metr_sec - g2.Metr_sec);
}

public override string ToString()
{
return $"metr_sec-{Metr_sec:0.00} km_ch " +
$"- {Km_ch:0.00} mil_ch - {Mil_ch:0.00} ";             // возвращаем необходимые значения
}
}
}