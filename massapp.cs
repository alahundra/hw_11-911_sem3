using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassaApp
{
public class Student
{
public int StudentId { get; set; }
public int GroupId { get; set; }
public string Name { get; set; }
public List<int> Marks { get; set; } = new List<int>();

public override string ToString()
{
return $"{StudentId} {Name} {Marks.Count()}"; 
}

}

public class Group
{
public int GroupId { get; set; }
public string GroupNum { get; set; }
private List<Student> Students { get; set; }     //cписок студентов
= new List<Student>();
public Student this[int id]
{
get
{
var st = Students.FirstOrDefault(             //проверка на находением студента с определнным ид в группе
s => s.StudentId == id);
if (st is null)
throw new ArgumentOutOfRangeException($"" +
$"Студента с id-{id} нет в этой группе");
return st;
}
set
{
var st = Students.FirstOrDefault(                   //если есть, то значение студента
s => s.StudentId == value.StudentId);
if (st != null)
Students[Students.IndexOf(st)] = value;
else
Students.Add(value);                             //иначе добавляем(по аналогии делается с остальным)
}
}
public Student this[string name]
{
get
{
var st1 = Students.FirstOrDefault(
s => s.Name == name);
if (st1 is null)
throw new ArgumentOutOfRangeException($"" +
$"Студента с именем-{name} нет в этой группе");
return st1;
}
set
{
var st1 = Students.FirstOrDefault(
s => s.Name == value.Name);
if (st1 != null)
Students[Students.IndexOf(st1)] = value;
else
Students.Add(value);
}
}

public override string ToString()
{
var sb = new StringBuilder();
sb.AppendLine($"Группа - {GroupNum}:");

Students.ForEach(s => sb.AppendLine(s.ToString()));

return sb.ToString();
}
}
}




