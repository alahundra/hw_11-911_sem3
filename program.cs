using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassaApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Skorost s1, s2, s3;                   //скорости 1,2,3, вводятся поля с групповым ид студентов, группой, маркс
            s1 = Skorost.CreateFromMetr_sec(100);
            s2 = Skorost.CreateFromKm_ch(100);
            s3 = Skorost.CreateFromMil_ch(100);
            Console.WriteLine(s1);
            Console.WriteLine(s2);
            Console.WriteLine(s3);
            Console.WriteLine(s1 + s2);
            Console.WriteLine(s3 - s2);
            Console.WriteLine(s1 - s2);

            Console.WriteLine();

            var gr = new Group()
            {
                GroupId = 1,
                GroupNum = "11-911"
            };

            gr[101] = new Student()
            {
                GroupId = 1,
                Name = "Иванов",
                StudentId = 101
            };

            gr[102] = new Student()
            {
                GroupId = 1,
                Name = "Петров",
                StudentId = 102,
                Marks = { 5, 4, 5 }
            };

            gr[103] = new Student()
            {
                GroupId = 1,
                Name = "Максимов",
                StudentId = 103
            };

            gr[104] = new Student()
            {
                GroupId = 1,
                Name = "Сидоров",
                StudentId = 104
            };

            gr[105] = new Student()
            {
                GroupId = 1,
                Name = "Пупкин",
                StudentId = 105,
                Marks = { 3, 3, 3 }
            };

            Console.WriteLine(gr);

            gr[103] = new Student()
            {
                GroupId = 1,
                Name = "Николаев",
                StudentId = 103,
                Marks = { 5, 4, 5 }
            };

            gr["Салихов"] = new Student()
            {
               GroupId = 1,
               Name = "Салихов",
               StudentId = 106,
               Marks = { 5 }
            };
        Console.WriteLine("*************************");

Console.WriteLine(gr);
}
}
}